import json
import requests
import dotenv
import os
import argparse
import sys
dotenv.load_dotenv()

parser = argparse.ArgumentParser()
parser.add_argument(
    '--defaultDue', help='Default due string for items without a due date')
args = parser.parse_args()

GITLAB_API_ENDPOINT = "https://gitlab.com/api/v4/"
TODOIST_API_ENDPOINT = "https://api.todoist.com/rest/v1/"

# Set up API tokens
gitlab_api_token = os.getenv("GITLAB_API_TOKEN")
todoist_api_token = os.getenv("TODOIST_API_TOKEN")

# Set up reusable headers for authentication
gitlab_headers = {"Authorization": "Bearer %s" % gitlab_api_token}
todoist_headers = {"Authorization": "Bearer %s" %
                   todoist_api_token, "Content-Type": "application/json"}

# Get a list of all projects
response = requests.get(TODOIST_API_ENDPOINT +
                        "projects", headers=todoist_headers)
projects = json.loads(response.text)

# Delete our placeholder project's tasks if they're there
found_placeholder = False
sync_project = None
for project in projects:
    if project["name"] == "GitLab sync":
        # We found the placeholder project, skip creating a new one later
        found_placeholder = True
        sync_project = project

        # Get the placeholder project's ID
        response = requests.get(TODOIST_API_ENDPOINT + "tasks",
                                params={
                                    "project_id": project["id"]
                                },
                                headers=todoist_headers
                                )

        # Delete all tasks from our placeholder project
        tasks = json.loads(response.text)
        for task in tasks:
            requests.delete(TODOIST_API_ENDPOINT + "tasks/%s" %
                            task["id"], headers=todoist_headers)

# Create our placeholder project if needed
if found_placeholder == False:
    response = requests.post(
        TODOIST_API_ENDPOINT + "projects",
        data=json.dumps({
            "name": "GitLab sync"
        }),
        headers=todoist_headers)
    sync_project = json.loads(response.text)

# Get our list of GitLab todos now
response = requests.get(GITLAB_API_ENDPOINT + "todos", headers=gitlab_headers)
gitlab_todos = json.loads(response.text)

# Copy each todo to Todoist
for todo in gitlab_todos:
    due = ""
    if ("due_date" in todo["target"]):
        due = todo["target"]["due_date"]
    else:
        due = args.defaultDue

    if (due is None):
        due = args.defaultDue

    requests.post(
        TODOIST_API_ENDPOINT + "tasks",
        data=json.dumps({
            "content": "[%s](%s)" % (todo["target"]["title"], todo["target_url"]),
            "project_id": sync_project["id"],
            "due_string": due
        }),
        headers=todoist_headers
    )
